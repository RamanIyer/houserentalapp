const mongoose = require('mongoose');

var visitSchema = new mongoose.Schema({
    userId: {
        type: String
    },
    visitDate: {
        type: String
    },
    houseId: {
        type: String
    }
});


mongoose.model('Visit', visitSchema);
