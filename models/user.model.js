const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: 'First name can\'t be empty'
    },
    lastName: {
        type: String,
        required: 'Last name can\'t be empty'
    },
    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },
    password: {
        type: String,
        required: 'Password can\'t be empty',
        minlength: [8,'Password must be atleast 8 characters long']
    },
    saltSecret: String
});

//Custom validations
userSchema.path('email').validate((val) => {
    emailRegex = /^[a-zA-z0-9#$_]+@[a-zA-Z0-9]+\.[a-zA-Z]+$/;
    return emailRegex.test(val);
}, 'Invalid e-mail');

//Events
userSchema.pre('save', function (next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});

//Methods
userSchema.methods.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.generateJwt = function () {
    return jwt.sign({ _id: this._id, firstName: this.firstName, lastName: this.lastName, email: this.email },
        process.env.JWT_SECRET);
};


mongoose.model('User', userSchema);
