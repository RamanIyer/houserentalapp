const mongoose = require('mongoose');

var houseSchema = new mongoose.Schema({
    houseName: {
        type: String
    },
    rooms: {
        type: Number
    },
    city: {
        type: String
    },
    ownerName: {
        type: String
    },
    contactNumber: {
        type: String
    }
});

mongoose.model('House', houseSchema);