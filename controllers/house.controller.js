const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const House = mongoose.model('House');

module.exports.getHouseList = (req, res, next) => {
    var filter = {};
    var inputCity = req.body.city && req.body.city != "" ? req.body.city : "";
    if (inputCity != "") {
        filter["city"] = inputCity;
    }
    var inputRooms = req.body.rooms && req.body.rooms != "" ? req.body.rooms : "";
    if (inputRooms != "") {
        filter["rooms"] = inputRooms;
    }
    House.aggregate(
        [{ $match: filter }]
    )
        .then(houses => {
            if (houses) {
                res.send(houses);
            }
        });
};

module.exports.getCities = (req, res, next) => {
    House.distinct('city')
        .then(cities => {
            if (cities) {
                res.json(cities);
            }
        });
};

module.exports.getRooms = (req, res, next) => {
    House.distinct('rooms')
        .then(rooms => {
            if (rooms) {
                res.json(rooms);
            }
        });
};