const mongoose = require('mongoose');

const Visit = mongoose.model('Visit');

module.exports.scheduleVisit = (req, res, next) => {
    var visit = new Visit();
    visit.userId = req.body.userId;
    visit.date = req.body.visitDate;
    visit.houseId = req.body.houseId;
    visit.save((err, doc) => {
        if (!err) {
            res.send(doc);
        }
        else {
            return next(err);
        }
    });
};