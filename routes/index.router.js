const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const ctrlHouse = require('../controllers/house.controller');
const ctrlVisit = require('../controllers/visit.controller');

const jwtHelper = require('../config/jwtHelper');

router.post('/register', ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.post('/getHouseList', jwtHelper.verifyJwtToken, ctrlHouse.getHouseList);
router.get('/getCities', jwtHelper.verifyJwtToken, ctrlHouse.getCities);
router.get('/getRooms', jwtHelper.verifyJwtToken, ctrlHouse.getRooms);
router.post('/scheduleVisit', jwtHelper.verifyJwtToken, ctrlVisit.scheduleVisit);

module.exports = router;