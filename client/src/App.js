import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Navbar from './components/Navbar'
import Landing from './components/Landing'
import Login from './components/Login'
import Register from './components/Register'
import Filter from './components/Filter'
import HouseList from './components/HouseList'
import ScheduleVisit from './components/ScheduleVisit'

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Navbar />
                    <Route exact path="/" component={Landing} />
                    <div className="container">
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/filter" component={Filter} />
                        <Route exact path="/houseList" component={HouseList} />
                        <Route exact path="/scheduleVisit" component={ScheduleVisit} />
                    </div>
                </div>
            </Router>
        )
    }
}

export default App
