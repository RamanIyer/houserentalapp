import axios from 'axios'

export const getCities = () => {
    return axios
        .get('http://localhost:5000/api/getCities', {
            headers: {
                'token': localStorage.usertoken
            }
        })
        .then(response => {
            return response.data;
        })
}

export const getRooms = () => {
    return axios
        .get('http://localhost:5000/api/getRooms', {
            headers: {
                'token': localStorage.usertoken
            }
        })
        .then(response => {
            return response.data;
        })
}

export const getHouseList = filter => {
    var headers = {
        'token': localStorage.usertoken
    };
    return axios
        .post('http://localhost:5000/api/getHouseList', filter, { "headers": headers })
        .then(response => {
            return response.data;
        })
        .catch(err => {
            console.log(err)
        })
}
