import React, { Component } from 'react'
import jwt_decode from 'jwt-decode'
import { getCities, getRooms } from './HouseFunction'

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            cities: [],
            selectedCity: "",
            rooms: [],
            selectedRoom: "",
            userId: ""
        }
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            userId: decoded._id
        })
        getCities()
            .then(res => {
                if (res) {
                    return res;
                }
            })
            .then(data => {
                var citiesFromApi = data.map(city => {
                    return { value: city, display: city };
                });
                this.setState({
                    cities: [
                        {
                            value: "",
                            display:
                                "(Select your city)"
                        }
                    ].concat(citiesFromApi)
                });
            })
        getRooms()
            .then(res => {
                if (res) {
                    return res;
                }
            })
            .then(data => {
                var roomsFromApi = data.map(room => {
                    return { value: room, display: room };
                });
                this.setState({
                    rooms: [
                        {
                            value: "",
                            display:
                                "(Select your room)"
                        }
                    ].concat(roomsFromApi)
                });
            })
    }

    onSubmit(e) {
        e.preventDefault()

        var filter = {
            city: this.state.selectedCity,
            rooms: this.state.selectedRoom
        };
        localStorage.setItem('filter', JSON.stringify(filter));
        this.props.history.push(`/houseList`);
    }

    render() {
        return (
            <div className="container">
                <div className="jumbotron">
                    <form noValidate onSubmit={this.onSubmit}>
                        <h1 className="h3 font-weight-normal">Filter</h1>
                        <div className="form-group">
                            <label htmlFor="email">City</label>
                            <select
                                value={this.state.selectedCity}
                                onChange={e =>
                                    this.setState({
                                        selectedCity: e.target.value,
                                        validationError: e.target.value === "" ? "You must select a city" : ""
                                    })
                                }
                            >
                                {this.state.cities.map(city => (
                                    <option
                                        key={city.value}
                                        value={city.value}
                                    >
                                        {city.display}
                                    </option>
                                ))}
                            </select>
                            <div style={{
                                color: "red",
                                marginTop: "5px"
                            }}>
                                {this.state.validationError}
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Rooms</label>
                            <select
                                value={this.state.selectedRoom}
                                onChange={e =>
                                    this.setState({
                                        selectedRoom: e.target.value,
                                        validationError: e.target.value === "" ? "You must select a room" : ""
                                    })
                                }
                            >
                                {this.state.rooms.map(room => (
                                    <option
                                        key={room.value}
                                        value={room.value}
                                    >
                                        {room.display}
                                    </option>
                                ))}
                            </select>
                            <div style={{
                                color: "red",
                                marginTop: "5px"
                            }}>
                                {this.state.validationError}
                            </div>
                            <button
                                type="submit"
                                className="btn btn-primary"
                            >
                                Filter
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Profile
