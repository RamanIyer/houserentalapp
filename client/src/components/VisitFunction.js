import axios from 'axios'

export const scheduleVisit = newSchedule => {
    var headers = {
        'token': localStorage.usertoken
    };
    return axios
        .post('http://localhost:5000/api/scheduleVisit', newSchedule, { "headers": headers })
        .then(response => {
            return response.data;
        })
        .catch(err => {
            console.log(err)
        })
}