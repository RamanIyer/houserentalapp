import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
import { getHouseList } from './HouseFunction'

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            houseList: [],
            city: "",
            rooms: ""
        }
        this.onScheduleVisit = this.onScheduleVisit.bind(this);
        localStorage.removeItem("houseId");
    }

    componentDidMount() {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            userId: decoded._id
        })
        var filter = JSON.parse(localStorage.filter);
        if (filter && filter.city) {
            this.setState({
                city: filter.city
            })
        }
        if (filter && filter.rooms) {
            this.setState({
                rooms: filter.rooms
            })
        }

        getHouseList(filter)
            .then(res => {
                if (res) {
                    return res;
                }
            })
            .then(data => {
                var housesFromApi = data.map(house => {
                    return { id: house._id, houseName: house.houseName, rooms: house.rooms, city: house.city, ownerName: house.ownerName, contactNumber: house.contactNumber };
                });
                this.setState({
                    houseList: housesFromApi
                });
            })
    }

    onScheduleVisit(id) {
        localStorage.setItem("houseId", id);
        this.props.history.push(`/scheduleVisit`);
    }

    render() {
        const HouseDetails = () => {
            var rows = [];
            this.state.houseList.map((data, i) => {
                rows.push(
                    <div key={data.id}>
                        <p>House Name :- {data.houseName}</p>
                        <p>Rooms:- {data.rooms}</p>
                        <p>City:- {data.city}</p>
                        <p>Owner Name:- {data.ownerName}</p>
                        <p>Contact Number:- {data.contactNumber}</p>
                        <button onClick={() => {
                            this.onScheduleVisit(data.id);
                        }}>Schedule Visit</button>
                    </div>
                );
            })
            return rows;
        }

        const FilterDetails = (
            <p> Filter Applied: {this.state.city != "" ? "City: " + this.state.city : ""} {this.state.rooms != "" ? "Rooms: " + this.state.rooms : ""}</p>
        )

        return (
            <div className="container">
                <div className="jumbotron">
                    <h1 className="h3 font-weight-normal">House List</h1>
                    {FilterDetails}
                    <Link to="/filter">
                        Go back to filter page..
                    </Link>
                    {HouseDetails()}
                </div>
            </div>
        )
    }
}

export default Profile
