import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
import { scheduleVisit } from './VisitFunction'

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            houseId: "",
            scheduleDate: Date.now()
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    componentDidMount() {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            userId: decoded._id
        })
        var houseId = localStorage.houseId;
        this.setState({
            houseId: houseId
        })
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        const newSchedule = {
            userId: this.state.userId,
            visitDate: this.state.scheduleDate,
            houseId: this.state.houseId
        }

        scheduleVisit(newSchedule).then(res => {
            this.props.history.push(`/houseList`)
        })
    }

    render() {
        return (
            <div className="container">
                <div className="jumbotron">
                    <form noValidate onSubmit={this.onSubmit}>
                        <h1 className="h3 font-weight-normal">Scehdule Visit</h1>
                        <label htmlFor="scheduleDate">Schedule Date</label>
                        <input
                            type="date"
                            className="form-control"
                            name="scheduleDate"
                            value={this.state.scheduleDate}
                            onChange={this.onChange}
                        />
                        <button
                            type="submit"
                            className="btn btn-primary"
                        >
                            Schedule
                        </button>
                        <Link to="/houseList">
                            Go back to house list...
                        </Link>
                    </form>
                </div>
            </div>
        )
    }
}

export default Profile
